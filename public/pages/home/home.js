(function(){

angular.module('hackathon')
	.controller('HomeController', HomeController)
	.controller('RegistrationModalController', RegistrationModalController)
	.controller('LoginModalController', LoginModalController);

HomeController.$inject = ['$scope', 'Request'];
function HomeController($scope, Request){
	Request.getAllAdvert().then(function(requests) {
		$scope.requests = requests;
	});
}

function RegistrationModalController($scope, $uibModalInstance,Request) {
	$scope.user = {
		firstname : '',
		lastname : '',
		email : '',
		password : ''
	};

	$scope.register = function() {
		if($scope.form.$valid)
			Request.register($scope.user).then(function () {
				$uibModalInstance.close();
			});
	}
}

function LoginModalController($scope, $uibModalInstance,Request) {
	$scope.user = {
		email : '',
		password : ''
	};

	$scope.login = function() {
		if ($scope.form.$valid)
			Request.login($scope.user).then(function () {
				$uibModalInstance.close();
			});
	}
}

})();
