(function () {

angular.module('hackathon')
	.controller('TwitterController', TwitterController);

TwitterController.$inject = ['$scope', '$http'];
function TwitterController($scope, $http) {
	$scope.users = [];

	$http.get('/api/twitter').then(function (response) {
		$scope.users = $scope.users.concat(response.data);
	});
}

})();
