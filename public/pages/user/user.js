(function () {

angular.module('hackathon')
	.controller('UserController', UserController);

UserController.$inject = ['$scope', '$rootScope', '$http', '$timeout'];
function UserController($scope, $rootScope, $http, $timeout) {
	$scope.updated = false;

	$scope.user = {
		firstname: $rootScope.user.firstname,
		lastname: $rootScope.user.lastname,
		email: $rootScope.user.email,
		phone: $rootScope.user.phone || '',
		schoolName: $rootScope.user.schoolName
	};

	$scope.update = function () {
		$http.patch('/users/' + $rootScope.user._id, $scope.user).then(function () {
			$scope.updated = true;

			$timeout(function () {
				$scope.updated = false;
			}, 3000);

			$rootScope.user = {
				_id: $rootScope.user._id,
				firstname: $scope.user.firstname,
				lastname: $scope.user.lastname,
				email: $scope.user.email,
				phone: $scope.user.phone,
				schoolName: $scope.user.schoolName
			};
		});
	}
}

})();
