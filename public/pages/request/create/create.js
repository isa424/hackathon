(function () {

angular.module('hackathon')
	.controller('CreateRequestController', CreateRequestController);

CreateRequestController.$inject = ['$scope','Request','$state', 'Upload'];
function CreateRequestController($scope,Request,$state, Upload) {
	$scope.data = {};

	$scope.createNewRequest = function () {
		if($scope.form.$valid) {
			Upload.upload({
				url: '/api/requests',
				data: {
					name: $scope.data.name,
					desc: $scope.data.desc,
					moneyAmount: $scope.data.moneyAmount,
					file: $scope.data.photo
				}
			}).then(function (response) {
				$state.go('app.request-details', {id: response.data._id});
			});
		} else {
			console.log("invalid")
		}
	}
}

})();
