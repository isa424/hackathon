(function(){

angular.module('hackathon')
	.controller('RequestDetailsController', RequestDetailsController);

RequestDetailsController.$inject = ['$scope', 'Request', '$stateParams','$uibModal'];
function RequestDetailsController($scope, Request, $stateParams,$uibModal) {
	var requestId = $stateParams.id;

	Request.getOneAdvert(requestId).then(function(response){
		$scope.request = response.data;
	});

	$scope.donation = function(){
		return $uibModal.open({
			templateUrl : 'pages/request/details/donation-modal.html',
			controller : 'DonationModalController',
			resolve : {
				params : function(){
					return {
						request : $scope.request
				}
			}
		}}).result.then(function (result) {
			if (result) {
				$scope.request.collected = parseInt($scope.request.collected) + parseInt(result);
			}
		});
	}


}

})();
