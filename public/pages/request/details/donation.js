(function () {

    angular.module('hackathon')
        .controller('DonationModalController', DonationModalController);

    DonationModalController.$inject = ['$scope', '$uibModalInstance', 'Request', 'params'];
    function DonationModalController($scope, $uibModalInstance, Request, params) {

        $scope.request = params.request

        $scope.donation = {
            moneyAmount : 0,
            card : {
                number : "",
                expiry : "",
                cvc : "",
                name : ""
            },
            user : {
                name : ""
            },
            advertId: params.request._id
        };

        $scope.cancel = function () {
            $uibModalInstance.close($scope.donation.moneyAmount);
        };

        $scope.createDonate = function () {
                Request.createDonation($scope.donation).then(function(){
                    $uibModalInstance.close($scope.donation.moneyAmount);
                });
        }

    }

})();
