(function(){

angular.module('hackathon')
	.controller('AppController', AppController);

AppController.$inject = ['$scope', '$uibModal', 'Request'];
function AppController($scope, $uibModal, Request){
	$scope.registration = function(){
		return $uibModal.open({
			templateUrl : 'pages/home/registration-modal.html',
			controller : 'RegistrationModalController',
			size : 'sm'
		});
	}

	$scope.openLoginForm = function(){
		return $uibModal.open({
			templateUrl : 'pages/home/login-modal.html',
			controller : 'LoginModalController',
			size : 'sm'
		});
	}

	$scope.logout = function () {
		Request.logout();
	}
}

})();
