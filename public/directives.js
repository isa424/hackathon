(function(){

angular.module('hackathon')
	.directive('fitHeight', fitHeight)
	.directive('minimalizaSidebar', minimalizaSidebar)
	.directive('pageTitle', pageTitle)
	.directive('sideNavigation', sideNavigation)
	.directive('fullScroll', fullScroll)
	.directive('icheck', icheck);


/**
 * fitHeight - Directive for set height fit to window height
 */
function fitHeight(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.css("height", $(window).height() + "px");
            element.css("min-height", $(window).height() + "px");
        }
    };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
*/
function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(500);
                        }, 100);
                } else if ($('body').hasClass('fixed-sidebar')){
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(500);
                        }, 300);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            };
        }
    };
}

/**
 * pageTitle - Directive for set Page title - meta title
 */
function pageTitle($rootScope, $timeout) {
	return {
		link: function(scope, element) {
			var title;

			var listener = function(event, toState, toParams, fromState, fromParams) {
				// Default title
				title = 'TKM';

				if(toState.data && toState.data.pageTitle){
					title = toState.data.pageTitle;
				}

				$timeout(function() {
					element.text(title);
				});
			};

			$rootScope.$on('$stateChangeStart', listener);
		}
	};
}

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function(){
                element.metisMenu();

            });
        }
    };
}

/**
 * fullScroll - Directive for slimScroll with 100%
 */
function fullScroll($timeout){
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
			$timeout(function(){
				setHeight();
			});

			element.slimscroll({
				height: '100%',
				railOpacity: 0.9
			});

			$(window).resize(setHeight);

			function setHeight(){
				var height;

				if(attr.viewHeight > 0){
					height = attr.viewHeight*1;
				}
				else{
					height = $(window).height() + attr.viewHeight*1;
				}

				element.height(height);
			}
        }
    };
}

function icheck() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function($scope, element, $attrs, ngModel) {
			var value;
			value = $attrs.value;

			$scope.$watch($attrs.ngModel, function(newValue){
				$(element).iCheck('update');
			});

			return $(element).iCheck({
				checkboxClass: 'icheckbox_flat-red',
				radioClass: 'iradio_flat-red'

			}).on('ifChanged', function(event) {
				if ($(element).attr('type') === 'checkbox' && $attrs.ngModel) {
					$scope.$apply(function() {
						return ngModel.$setViewValue(event.target.checked);
					});
				}
				if ($(element).attr('type') === 'radio' && $attrs.ngModel) {
					return $scope.$apply(function() {
						return ngModel.$setViewValue(value);
					});
				}
			});
		}
	};
}

})();
