/**
 * This is the main js file. IOT app is define here with config and run blocks.
 */

(function(){

angular.module('hackathon', [
	'ui.router',
	'ui.bootstrap',
	'oc.lazyLoad',
	'ngFileUpload'
])
.config(config)
.run(run);

config.$inject = ['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$locationProvider'];
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $locationProvider){
	$locationProvider.html5Mode(false);
	$urlRouterProvider.otherwise("/");
	$ocLazyLoadProvider.config({
		debug	: false
	});

	$stateProvider
		.state('app', {
			url			: '/',
			abstract	: true,
			controller	: "AppController",
			templateUrl	: "views/common/content_top_navigation.html"
		})
		.state('app.home', {
			url			: '',
			templateUrl : "pages/home/home.html",
			controller	: "HomeController",
			data		: { pageTitle: 'School Funder'}
		})
		.state('app.new-request', {
			url			: 'request/form',
			templateUrl : "pages/request/create/create.html",
			controller  : "CreateRequestController",
			data 		: {}
		})
		.state('app.request-details', {
			url			: 'request/details/:id',
			templateUrl : "pages/request/details/details.html",
			controller  : "RequestDetailsController",
			data 		: {}
		})
		.state('app.profile', {
			url			: 'profile',
			templateUrl	: 'pages/user/user.html',
			controller	: 'UserController'
		})
		.state('app.twitter', {
			url			: 'admin/twitter',
			templateUrl	: 'pages/twitter/twitter.html',
			controller	: 'TwitterController'
		});
}

run.$inject = ['$state', '$rootScope'];
function run($state, $rootScope){
	$rootScope.user = null;

	if (user._id)
		$rootScope.user = user;
	// Make state data available around the app and in html code
	$rootScope.$state = $state;
}

})();
