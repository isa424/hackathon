(function () {

	angular.module('hackathon')
		.factory('Request', RequestProvider);


	/**
	 * This provider defines Request model.
	 */
	RequestProvider.$inject = ['$q', '$http', '$rootScope'];
	function RequestProvider($q, $http, $rootScope) {

		function Request(details) {
			_.extend(this, details);
		}

		//---------- Static Functions ----------

		/**
		 * Gets all hackathon requests that user can access.
		 *
		 * @return {Promise} Resolved to array of Request instances.
		 */

		/*
		Request.all = function () {
			// TODO $q.resolve will be replaced with $http. For now, return dummy data.
			return $q.resolve([
				{
					_id: 1,
					name: 'Okulumuza Kütüphane Kurmak İstiyoruz',
					desc: 'Merhabalar Biz Cizreden Kebeli ortaokulu olarak bir kütüphane kurmak istiyoruz. Bunun için yardım ve desteklerinize ihtiyacımız var .Ayrıca projeksiyon ve bilgisayar ihtiyacımız da var.',
					photo: 'http://www.koyokullariyardimprojesi.org/wp-content/uploads/2017/11/slider-3-300x200.jpg',
					moneyAmount: '2000',
					createdBy: {
						firstname: 'Mehmet',
						lastname: 'Ertaş',
						school: 'Cizre Kebeli Ortaokulu',
						email: 'mehmet@kebeli.k12.tr',
						phone: ' 0505 151 50 81'
					}
				},
				{
					_id: 2,
					name: 'Dersliklerimizin Tadilatı ve Kırtasiye Yardımı',
					desc: 'Merhaba.Şanlıurfa ili Viranşehir ilçesine bağlı Sağlamlar İlkokulu’nda görev yapmaktayım.Okulum birleştirilmiş sınıf olup toplamda 41 öğrencim bulunmaktadır.(23 kız-18 erkek) Siz değerli yardımseverlerden okulum için ;kitaplık ve o kitaplığı dolduracak ilkokul seviyesine uygun okuma kitaplarına, öğrencilerim için kırtasiye malzemelerine, serbest etkinlik derslerinde kullanmak için eğitici oyunlara , masa örtüsü ve perdeye ihtiyacımız vardır. Şimdiden teşekkür ederim.',
					photo: 'http://www.koyokullariyardimprojesi.org/wp-content/uploads/2017/11/unnamed-2-300x200.jpg',
					moneyAmount: '3000',
					createdBy: {
						firstname: 'Osman',
						lastname: 'Mutlu',
						school: 'Viranşehir Sağlamlar İlkokulu',
						email: 'osman@saglamlar.k12.tr',
						phone: ' 0535 751 12 56'
					}
				},
				{
					_id: 3,
					name: 'Seldek İlkokuluna Destek',
					desc: 'İyi̇ günler.Şanliurfa’da çalişan bi̇r köy öğretmeni̇yi̇m. Okulumuzda 40 tane 1.Si̇ni̇f öğrenci̇leri̇mi̇z var. Koy okulu oldugumuz icin okulumuzda fazlaca kirtasi̇ye ve spor malzemeleri̇ eksi̇ğİmi̇z bulunmakta.Ri̇camiz okulumuza destek olmaniz. Fazlaca kirtasi̇ye ve spor malzemeleri̇ gonderebilirseniz çok sevi̇ni̇ri̇z. Teşekkürler.',
					photo: 'http://www.koyokullariyardimprojesi.org/wp-content/uploads/2017/11/unnamed-1-300x200.jpg',
					moneyAmount: '750',
					createdBy: {
						firstname: 'Yusuf',
						lastname: 'Sertel',
						school: 'Şanlıurfa Seldek İlkokulu',
						email: 'yusuf@seldek.k12.tr',
						phone: ' 0541 781 92 59'
					}
				}
			]).then(function (requests) {
				return _.map(requests, function (request) {
					return new Request(request);
				});
			});
		};


		Request.getOneAdvert = function (id) {
			return Request.all().then(function (requests) {
				return requests.find(function (req) {
					return req._id == id;
				});
			});
		}

		*/

		/**
		 *  Advert 
		 */

		Request.getAllAdvert = function () {
			 return $http.get("/api/requests")
				.then(function (requests) {
					return _.map(requests.data, function (request) {
						return new Request(request);
					});
				});
		}

		Request.getOneAdvert = function (id) {
			return $http.get("/api/requests/"+id)
		}

		Request.createNewAdvert = function (params) {
			return $http.post("/api/requests", params)
		};

		Request.updateAdvert = function (params,id) {
			return $http.put("/api/requests/"+id,params,id)
		}

		Request.deleteAdvert = function (id) {
			return $http.delete("/api/requests/"+id)
		}


		/**
		 *  Donations
		 */

		Request.getDonations = function (id) {
			return $http.get("/api/donations/"+id)
		}


		Request.createDonation = function (params) {
			return $http.post("/api/donations",params)
		}

		Request.deleteDonation = function (id) {
			return $http.delete("/api/donations/"+id)
		}


		/**
		 *  Users
		 */
		Request.register = function (params) {
			return $http.post("/users/register", params).then(function (result) {
				$rootScope.user = result.data;
			});
		};

		Request.login = function (params) {
			return $http.post("/users/login", params).then(function (result) {
				$rootScope.user = result.data;
			});
		};

		Request.logout = function () {
			return $http.post('/users/logout').then(function () {
				$rootScope.user = {};
			});
		};

		return Request;
	}

})();
