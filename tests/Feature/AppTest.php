<?php

namespace Tests\Feature;

use App\Advert;
use App\User;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Tests\TestCase;
//use Illuminate\Foundation\Testing\WithoutMiddleware;
//use Illuminate\Foundation\Testing\DatabaseMigrations;
//use Illuminate\Foundation\Testing\DatabaseTransactions;

class AppTest extends TestCase
{
	public function tests_main_func()
	{
		$advert = Advert::create(['title' => 'Falan']);

		$response = $this->json('get', 'api/adverts/' . $advert->id);
		$response->assertStatus(200);

		$result = Advert::find($advert->id);
		$this->assertNotNull($result);
	}

	public function tests_register()
	{
		$data = [
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'email' => 'falan@mail.com',
			'password' => 'password'
		];

		$response = $this->json('post', '/users/register', $data);
		$user = User::where('email', $data['email'])->first();
		$response->assertCookie('sessionId', $user->sessionId);
		$response->assertStatus(200);
	}

	public function test_login()
	{
		$data = [
			'email' => 'falan@mail.com',
			'password' => 'password'
		];
		$this->post('/users/register', $data);

		$response = $this->json('post', '/users/login', $data);
		$user = User::where('email', $data['email'])->first();
		$response->assertCookie('sessionId', $user['sessionId']);
		$response->assertStatus(200);
	}

	public function test_need_to_be_logged_out()
	{
		$user = User::create([
			'email' => 'falan',
			'password' => password_hash('password', 1),
			'sessionId' => str_random()
		]);

		$response = $this->call(
			'post', '/users/register',
			['email' => 'email', 'password' => 'password'],
			['sessionId' => Crypt::encrypt($user['sessionId'])]
		);
		$response->assertStatus(401);
	}

	public function test_need_to_be_logged_in()
	{
		$response = $this->post('/users/logout');
		$response->assertStatus(401);
	}

	public function tearDown()
	{
		User::getQuery()->delete();
		Advert::getQuery()->delete();
	}
}
