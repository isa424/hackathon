<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'requests'], function () {
	Route::get('/', 'AdvertController@getAll');
	Route::get('/{id}', 'AdvertController@get');
	Route::post('/', 'AdvertController@create');
	Route::patch('/{id}', 'AdvertController@update');
	Route::delete('/{id}', 'AdvertController@delete');
});

Route::group(['prefix' => 'donations'], function () {
	Route::get('/{id}', 'DonationController@get');
	Route::post('/', 'DonationController@create');
	Route::delete('/{id}', 'DonationController@delete');
});

Route::group(['prefix' => 'twitter'], function () {
	Route::get('/', 'TwitterController@getAll');
});
