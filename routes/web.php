<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function (\Illuminate\Http\Request $request) {
	if ($request->cookie('sessionId')) {
		$user = \App\User::where('sessionId', $request->cookie('sessionId'))->first();
	}

	return view('welcome', ['user' => $user ?? []]);
});

Route::middleware(['guest'])->group(function () {
	Route::group(['prefix' => 'users'], function () {
		Route::post('/register', 'UserController@register');
		Route::post('/login', 'UserController@login');
	});
});

Route::middleware(['loggedIn'])->group(function () {
	Route::post('/users/logout', 'UserController@logout');
	Route::patch('/users/{id}', 'UserController@update');
});
