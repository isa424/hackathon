<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\TwitterUser;
use Twitter;

class UpdateTwitterUsers extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'twitter:update-users';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$keywords = ['kitap', 'köy', 'okul', 'yardım', 'kütüphane', 'kırtasiye', 'kışlık', 'giysi'];

		// Get the latest tweet id in the list
		$latestTweetId = TwitterUser::orderBy('latestTweetId', 'desc')->first()->latestTweetId;

		$result = Twitter::getSearch([
			'q' => 'okul yardım',
			'result_type' => 'recent',
			'since_id' => $latestTweetId,
			'count' => 1000
		]);

		echo count($result->statuses)."\n";

		if(empty($result->statuses)) {
			return;
		}

		foreach($result->statuses as $tweet) {
			// Check if user already exists and don't save it if exists
			$tu = TwitterUser::where('twitter_id', $tweet->user->id)->first();

			if($tu) {
				// If there is a new tweet, push it to posts ist
				$exists = !!collect($tu->posts)->first(function($t) use($tweet){
					(string)$t['id'] == (string)$tweet->id;
				});
				if(!$exists) {
					$frequency = 0;
					$text = mb_strtolower($tweet->text);
					foreach($keywords as $keyword) {
						if(mb_strpos($text, $keyword) !== false) {
							$frequency++;
						}
					}

					$tu->push('posts', [
						'id' => $tweet->id,
						'text' => $tweet->text,
						'frequency' => $frequency
					]);
					$tu->score = $tu->score + $frequency;
					if($tu->latestTweetId < $tweet->id) {
						$tu->latestTweetId = $tweet->id;
					}
					$tu->save();
				}
				continue;
			}

			$tu = new TwitterUser();
			$tu->twitter_id = $tweet->user->id;

			$frequency = 0;
			$text = mb_strtolower($tweet->text);
			foreach($keywords as $keyword) {
				if(mb_strpos($text, $keyword) !== false) {
					$frequency++;
				}
			}

			$tu->posts = [['id' => $tweet->id, 'text' => $tweet->text, 'frequency' => $frequency]];
			$tu->score = $frequency;
			$tu->details = $tweet->user;
			$tu->latestTweetId = $tweet->id;
			$tu->save();
		}
	}
}
