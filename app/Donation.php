<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Donation extends Model
{
	protected $fillable = ['moneyAmount', 'userId', 'advertId'];
}
