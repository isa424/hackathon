<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class TwitterUser extends Model
{
	protected $fillable = [
		'posts', 'totalAnalyzedPostCount', 'name', 'screen_name', 'twitter_id', 'followers_count', 'friends_count', 'profile_image_url'
	];
}
