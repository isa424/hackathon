<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Donation;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use MongoDB\BSON\ObjectId;

class UserController extends Controller
{
	public function getDonations(string $id)
	{
		$donations = Donation::where('userId', new ObjectId($id))->get();
		$advertIds = $donations->map(function ($donation){
			return new ObjectId($donation->advertId);
		});
		$adverts = Advert::whereIn('_id', $advertIds);
		$donations = $donations->map(function ($donation) use ($adverts) {
			$donation->advert = $adverts->first(function ($advert) use ($donation) {
				return (string) $advert->id === (string) $donation->advertId;
			});
		});

		return $donations;
	}

	public function register(Request $request)
	{
		$data = array_only($request->all(), ['firstname', 'lastname', 'email', 'phone', 'password']);
		$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
		$data['sessionId'] = str_random();
		$data['expiry'] = Carbon::now()->addDay();

		$user = User::create($data);

		$response = new Response($user);
		return $response->withCookie(Cookie::make('sessionId', $data['sessionId']));
	}

	public function login(Request $request)
	{
		$credentials = array_only($request->all(), ['email', 'password']);

		$user = User::where('email', $credentials['email'])->first();

		if (!password_verify($credentials['password'], $user['password']))
			return response()->json('User not found!', 400);

		$user->sessionId = str_random();
		$user->expiry = Carbon::now()->addDay();
		$user->save();

		$response = new Response($user);
		return $response->withCookie(Cookie::make('sessionId', $user->sessionId));
	}

	public function logout(Request $request)
	{
		Cookie::forget('sessionId');
		return User::where('sessionId', $request->cookie('sessionId'))
			->update(['sessionId' => null]);
	}

	public function update(string $id, Request $request)
	{
		$update = array_only($request->all(), ['firstname', 'lastname', 'email', 'phone', 'schoolName']);

		$result = User::where('_id', new ObjectId($id))->update($update);

		return $result;
	}
}
