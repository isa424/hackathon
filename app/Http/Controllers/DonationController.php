<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Donation;
use MongoDB\BSON\ObjectId;

class DonationController extends Controller
{
	public function get(string $id)
	{
		return Donation::find($id);
	}

	public function create(Request $request)
	{
		if (!$request->hasCookie('sessionId')) {
			return false;
		}

		if ($request->hasCookie('sessionId')) {
			$user = User::where('sessionId', $request->cookie('sessionId'))->first();

			if (!$user)
				return response()->json('User is required!', 400);
		}

		$data = $request->all();
		$data['moneyAmount'] = (int) $data['moneyAmount'];
		$data['advertId'] = new ObjectId($data['advertId']);
		$data['userId'] = new ObjectId($user->id);
		$result = Donation::create($data);
		return $result;
	}

	public function delete(string $id)
	{
		$result = Donation::delete($id);

		return $result;
	}
}
