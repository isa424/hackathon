<?php

namespace App\Http\Controllers;

use App\TwitterUser;
use Illuminate\Http\Request;

class TwitterController extends Controller
{
	public function getAll()
	{
		return TwitterUser::all();
	}
}
