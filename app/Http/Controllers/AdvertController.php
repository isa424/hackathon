<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Donation;
use App\User;
use Illuminate\Http\Request;
use MongoDB\BSON\ObjectId;
use Twitter;

class AdvertController extends Controller
{

	public function getAll(Request $request)
	{	$user = null;
		if ($request->cookie('sessionId'))
			$user = User::where('sessionId', $request->cookie('sessionId'))->first();

		$adverts = Advert::all();
		$adverts->each(function ($advert) {
			$advert->collected = Donation::where('advertId', new ObjectId($advert->id))->sum('moneyAmount');
		});

		if (isset($user))
			$adverts->each(function ($advert) use ($user) {
				$advert->userSum = Donation::where('userId', new ObjectId($user->id))
					->where('advertId', new ObjectId($advert->id))->sum('moneyAmount');
			});

		return $adverts;
	}

	public function get(string $id, Request $request)
	{
		$user = null;
		if ($request->cookie('sessionId'))
			$user = User::where('sessionId', $request->cookie('sessionId'))->first();

		$advert = Advert::find($id);
		$advert->collected = Donation::where('advertId', new ObjectId($advert->id))->sum('moneyAmount');
		$advert->createdBy = User::find($advert->createdBy);

		if ($user)
			$advert->userSum = Donation::where('advertId', new ObjectId($user->id))
				->where('advertId', new ObjectId($advert->id))->sum('amount');

		return $advert;
	}

	public function create(Request $request)
	{
		if (!$request->hasCookie('sessionId')) {
			return false;
		}

		if ($request->hasCookie('sessionId')) {
			$user = User::where('sessionId', $request->cookie('sessionId'))->first();

			if (!$user)
				return response()->json('User is required!', 400);
		}

		$data = array_only($request->all(), ['name', 'desc', 'moneyAmount']);
		$data['createdBy'] = new ObjectId($user->id);

		$request->file('file')->storeAs('public', $request->file('file')->getClientOriginalName());

		$data['photo'] = 'storage/'.$request->file('file')->getClientOriginalName();
		$result = Advert::create($data);

		$file = Twitter::uploadMedia(['media_data' => base64_encode(file_get_contents($request->file('file')->getRealPath()))]);
		$message = $user['schoolName']." yardım bekliyor! @isa4244, bağış yaparak veya paylaşarak destekle. https://google.com";
		Twitter::postTweet(['status' => $message, 'format' => 'json', 'media_ids' => $file->media_id]);

		return $result;
	}

	public function update(string $id, Request $request)
	{
		$data = array_only($request->all(), ['name', 'desc', 'moneyAmount', 'photo']);

		if ($request->hasFile('photo') && !$request->has('photo')) {
			$path = $request->file('photo')->storeAs('public', $request->file('photo')->getClientOriginalName());
			$data['photo'] = $path;
		}

		if ($request->has('photo'))
			$data['photo'] = null;

		$result = Advert::where('_id', new ObjectId($id))->update($data);

		return $result;
	}

	public function delete(string $id)
	{
		Donation::where('advertId', new ObjectId($id))->delete();
		$result = Advert::delete($id);

		return $result;
	}
}
