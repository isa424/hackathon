<?php

namespace App\Http\Middleware;

use App\User;
use Carbon\Carbon;
use Closure;

class AuthRequired
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->cookie('sessionId')) {
			$exist = User::where('sessionId', $request->cookie('sessionId'))
				->exists();

			if ($exist)
				return $next($request);

			return response()->json('Authentication Required!', 401);
		} else {
			return response()->json('Authentication Required!', 401);
		}
	}
}
