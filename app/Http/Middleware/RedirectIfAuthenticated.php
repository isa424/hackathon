<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param  string|null $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		if ($request->cookie('sessionId')) {
			$user = User::where('sessionId', $request->cookie('sessionId'))->first();

			if($user)
				return response()->json('You need to be logged out!', 401);
		}

		return $next($request);
	}
}
