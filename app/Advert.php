<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Advert extends Model
{
	protected $fillable = ['name', 'desc', 'moneyAmount', 'photo', 'createdBy', 'collected'];
}
