<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <!-- bower:css -->
    <link rel="stylesheet" href="bower/bootstrap.min.css">
    <link rel="stylesheet" href="bower/metisMenu.css">
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/custom.css">
    <link rel="stylesheet" href="app.css">
	<link rel="stylesheet" href="pages/home/home.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

	<!-- bower:js -->
	<script src="bower/jquery.js"></script>
	<script src="bower/angular.js"></script>
	<script src="bower/angular-ui-router.js"></script>
	<script src="bower/bootstrap.min.js"></script>
	<script src="bower/metisMenu.js"></script>
	<script src="bower/ocLazyLoad.js"></script>
	<script src="bower/ui-bootstrap-tpls.js"></script>
	<script src="bower/underscore.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script src="app.js"></script>
	<script src="pages/home/home.js"></script>
	<script src="js/models/customs-region.js"></script>
	<script src="js/inspinia.js"></script>
	<script src="directives.js"></script>
	<script src="app.controller.js"></script>
	<!-- endinject -->
</body>
</html>
