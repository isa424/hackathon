<!DOCTYPE html>
<html ng-app="hackathon">
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Okul Fonlayıcı</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bower:css -->
    <link rel="stylesheet" href="bower/bootstrap.min.css">
    <link rel="stylesheet" href="bower/metisMenu.css">
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/custom.css">
    <link rel="stylesheet" href="app.css">
    <link rel="stylesheet" href="pages/home/home.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <!-- endinject -->
</head>
<body>
    <div ui-view></div>

	<!-- bower:js -->
	<script src="bower/jquery.js"></script>
	<script src="bower/angular.js"></script>
	<script src="bower/angular-ui-router.js"></script>
	<script src="bower/bootstrap.min.js"></script>
	<script src="bower/metisMenu.js"></script>
	<script src="bower/ocLazyLoad.js"></script>
	<script src="bower/ui-bootstrap-tpls.js"></script>
	<script src="bower/underscore.js"></script>
	<script src="js/ng-file-upload.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	@isset($user)
		<script type="text/javascript">
			var user = {!! json_encode($user) !!};
		</script>
	@endisset
	<script src="app.js"></script>
	<script src="pages/home/home.js"></script>
	<script src="pages/request/create/create.js"></script>
	<script src="pages/request/details/donation.js"></script>
	<script src="pages/user/user.js"></script>
	<script src="pages/twitter/twitter.js"></script>
	<script src="js/models/request.js"></script>
	<script src="pages/request/details/details.js"></script>
	<script src="js/inspinia.js"></script>
	<script src="directives.js"></script>
	<script src="app.controller.js"></script>
	<!-- endinject -->
</body>
</html>
