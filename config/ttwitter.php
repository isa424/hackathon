<?php

// You can find the keys here : https://apps.twitter.com/

return [
	'debug'               => function_exists('env') ? env('APP_DEBUG', false) : false,

	'API_URL'             => 'api.twitter.com',
	'UPLOAD_URL'          => 'upload.twitter.com',
	'API_VERSION'         => '1.1',
	'AUTHENTICATE_URL'    => 'https://api.twitter.com/oauth/authenticate',
	'AUTHORIZE_URL'       => 'https://api.twitter.com/oauth/authorize',
	'ACCESS_TOKEN_URL'    => 'https://api.twitter.com/oauth/access_token',
	'REQUEST_TOKEN_URL'   => 'https://api.twitter.com/oauth/request_token',
	'USE_SSL'             => true,

	'CONSUMER_KEY'        => function_exists('env') ? env('TWITTER_CONSUMER_KEY', 'cjmAhpblSSfHJ7ClpkkCDNnkB') : 'cjmAhpblSSfHJ7ClpkkCDNnkB',
	'CONSUMER_SECRET'     => function_exists('env') ? env('TWITTER_CONSUMER_SECRET', 'vRQtkpzHI09Hs7XtU5lLyXyiOnCgTfxyzLfcLQgAcBdHp9F0nF') : 'vRQtkpzHI09Hs7XtU5lLyXyiOnCgTfxyzLfcLQgAcBdHp9F0nF',
	'ACCESS_TOKEN'        => function_exists('env') ? env('TWITTER_ACCESS_TOKEN', '955878078668042242-Bc8bL8iVjFUQoa6Wl7bO57Kxzvzfiu9') : '955878078668042242-Bc8bL8iVjFUQoa6Wl7bO57Kxzvzfiu9',
	'ACCESS_TOKEN_SECRET' => function_exists('env') ? env('TWITTER_ACCESS_TOKEN_SECRET', '11zH5MhAC0LMfzpsHzPKgQ4oxjfmK39GPuBOlsZVZNP9S') : '11zH5MhAC0LMfzpsHzPKgQ4oxjfmK39GPuBOlsZVZNP9S',
];
